#!/bin/bash

echo "172.17.0.1   host.docker.internal" >> /etc/hosts 

cd /var/www/app
umask 002

chown -R 33:33 storage
chown -R 33:33 .env
chown -R 33:33 bootstrap/cache
chown -R 33:33 public


apachectl -D FOREGROUND